import chess
import cv2
import pathlib
import os
import numpy as np
import keyboard
import time
import pyttsx3
from skimage.metrics import structural_similarity as compare_ssim
from itertools import permutations

ROOT = pathlib.Path(__file__).parent.parent.resolve()
INPUT = ROOT.joinpath("input")
#birdseye_dir = INPUT.joinpath("2D","2d_board_Moment.jpg")
birdseye_dir = INPUT.joinpath("2D","clean_board.png")
class ChessboardNotFoundError(Exception):
    pass

def cropSquares(squares, frame):
    
    indivudualSquares = {}
    for key in squares.keys():
        #white = squares[key][1]
        tl = (squares[key][0])
        br = (squares[key][1])
        #cent = (tl[0]+45, tl[1]+45)
        crop_img = frame[tl[1]:br[1], tl[0]:br[0]]
        #crop_img = cv2.cvtColor(crop_img,cv2.COLOR_BGR2HSV)
        indivudualSquares[key] = crop_img

    return indivudualSquares

def generateBoard(width):
    gap = width // 8
    squares = {}
    hori = ['a','b','c','d','e','f','g','h']
    vert = ['8','7','6','5','4','3','2','1']
    #squareCol = 0
    for i in range(8):
        for j in range(8):
            # if squareCol % 2 == 0:
            #     white = True
            # else:
            #     white = False
            tl= (gap*i, gap*j)
            br = (gap+gap*i, gap+gap*j)
            id = str(hori[i]+vert[j])
            squares[id] = (tl, br)#,white
            #squareCol += 1
    return squares

def detectDiff(imageA,imageB):
    # load the two input images
    # convert the images to grayscale
    frame_LUVA = cv2.cvtColor(imageA, cv2.COLOR_BGR2LUV)
    frame_thresholdA = cv2.inRange(frame_LUVA, (25, 0, 0), (100, 255, 255))
    frame_LUVB = cv2.cvtColor(imageB, cv2.COLOR_BGR2LUV)
    frame_thresholdB = cv2.inRange(frame_LUVB, (25, 0, 0), (100, 255, 255))

    hsvA = cv2.cvtColor(imageA, cv2.COLOR_BGR2HSV)
    hsvB = cv2.cvtColor(imageB, cv2.COLOR_BGR2HSV)
    grayA = cv2.cvtColor(hsvA, cv2.COLOR_BGR2GRAY)
    grayB = cv2.cvtColor(hsvB, cv2.COLOR_BGR2GRAY)
    # compute the Structural Similarity Index (SSIM) between the two
    # images, ensuring that the difference image is returned
    score1, diff = compare_ssim(frame_thresholdA, frame_thresholdB, full=True)
    score2, diff = compare_ssim(grayA, grayB, full=True)
    return score1, score2

def getScores(img1, img2, squares):
    scores_arr = []
    positions1 = cropSquares(squares, img1)
    positions2 = cropSquares(squares, img2)
    for key in positions1:
        score1, score2 = detectDiff(positions1[key],positions2[key])
        if score1 < score2:
            bigger = score2
        else:
            bigger = score1
        tup = (bigger, key)
        scores_arr.append(tup)
    return scores_arr

def getMove(scores_arr, board):
    scores_arr.sort(key=lambda x:x[0])
    #print("turn {}".format(i))
    # if i == 0:
    #     print(scores_arr_arr)
    top_10=[]
    for j in range(30):
        top_10.append(scores_arr[j][1])
    # Get all permutations of [1, 2, 3]
    index = 1
    no_move_found = True
    legal_perms = []
    moveMade = None
    while index<=29 and no_move_found:
        index+=1
        perm = permutations(top_10[:index], 2)
        legal_perms = []
        for p in perm:
            move = (str(p[0])+str(p[1]))
            if chess.Move.from_uci(move) in board.legal_moves:
                legal_perms.append(move)
                firstSquare = move[0:2]
                secondSquare = move[2:4]
                movedPiece = board.piece_at(chess.parse_square(firstSquare))
                movedName = chess.piece_name(movedPiece.piece_type)
                moveMade = movedName +" from " +firstSquare + " to "+secondSquare
                
                no_move_found = False
                break
    if not moveMade:
        return None
    else:
        board.push_san(move)
        

        return moveMade

def detectMove(prev, curr, squares):
    scores_arr = getScores(prev,curr,squares)
    sco = []
    for score in scores_arr:
        sco.append(score[0])
    avg = np.mean(sco)
    return avg



def warp2D(grid37, im_src, im_dst):
    # Four corners of the book in source image
    pts_src = np.array([grid37[0][0][0], grid37[0][2][0], grid37[6][0][0],grid37[6][2][0]])
    # Four corners of the book in destination image.
    pts_dst = np.array([[91, 450],[91, 270],[631, 450],[631, 270]])
    # Calculate Homography
    h, status = cv2.findHomography(pts_src, pts_dst)
    # Warp source image to destination based on homography
    im_out = cv2.warpPerspective(im_src, h, (im_dst.shape[1],im_dst.shape[0]))
    # Display images
    #cv2.imshow("Source Image", im_src)
    #cv2.imshow("Destination Image", im_dst)
    #cv2.imshow("Warped Source Image", im_out)
    return im_out

def main(vid=None):
    if vid:
        cap = cv2.VideoCapture(os.path.join(INPUT, vid))
    else:
        cap = cv2.VideoCapture(0)
    if (cap.isOpened()==False):
        print("can't read")
    ret1 = False
    while(cap.isOpened() & ret1==False):
        # Capture frame-by-frame
        ret, frame = cap.read()
        cv2.imshow("raw footage", frame)
        cv2.waitKey(25)
        """Find subpixel chessboard corners in image."""
        temp = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        ret1, corners = cv2.findChessboardCorners(temp,
                                                 (3, 7))
        if not ret1:
            #raise ChessboardNotFoundError("No chessboard could be found.")
            continue
        cv2.cornerSubPix(temp, corners, (11, 11), (-1, -1),
                         (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS,
                          30, 0.01))
        #fnl = cv2.drawChessboardCorners(frame, (3, 7), corners, ret)
    #cap.release()
    file = open("input/grid37.txt", "w")
    
    
    grid37 = np.array_split(corners,7)
    str_grid = repr(grid37)
    file.write("grid37 = " + str_grid + "\n")
    file.close()
    im_dst = cv2.imread(str(birdseye_dir))
    if vid:
        cap = cv2.VideoCapture(os.path.join(INPUT, vid))
    else:
        cap = cv2.VideoCapture(0)
    if (cap.isOpened()==False):
        print("can't read")

    white_turn = True
    black_turn = False

    firstLoop = True
    #time.sleep(5)
    #out = cv2.VideoWriter('output/noKeyboardBird.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 1, (722,722))
    raw = cv2.VideoWriter('output/noKeyboardRaw.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 1, (640,480))
    squares = generateBoard(722)
    board = chess.Board()
    engine = pyttsx3.init()
    engine.say("Game started.")
    engine.runAndWait()
    while(cap.isOpened()):
        # Capture frame-by-frame
        if firstLoop == True:
            birdseyeView = warp2D(grid37,frame,im_dst)
            lastMove = birdseyeView
            moving = False
            prevFrame = birdseyeView
            firstLoop = False
            #out.write(birdseyeView)
            raw.write(frame)
        else:

            ret, frame = cap.read()
            if ret == True:
                birdseyeView = warp2D(grid37,frame,im_dst)
                cv2.imshow("raw footage", frame)
                cv2.imshow("birdseye",birdseyeView)

                stable = detectMove(prevFrame, birdseyeView, squares)
                print(stable)
                if not moving and stable < 0.81:
                    moving = True
                    engine.say("Movement detected")
                    engine.runAndWait()
                if moving and stable > 0.87:
                    moving = False
                    engine.say("Board stable")
                    engine.runAndWait()
                    scores_arr = getScores(lastMove, birdseyeView, squares)
                    moveMade = getMove(scores_arr, board)
                    if moveMade == None:
                        engine.say("No legal moves detected")
                        engine.runAndWait()
                    else:
                        #out.write(birdseyeView)
                        print(moveMade)
                        engine.say(moveMade)
                        engine.runAndWait()
                        lastMove = birdseyeView
                if board.is_check():
                    engine.say("Check!")
                    engine.runAndWait()
                    print("Check")
                if board.is_checkmate():
                    engine.say("Checkmate!")
                    engine.runAndWait()
                    break
                    
                prevFrame = birdseyeView
                #out.write(birdseyeView)
                raw.write(frame)
                if cv2.waitKey(1000) & 0xFF == ord('q'):
                    break
                # Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
                
            else:
                break
    cap.release()
    #out.release()
    raw.release()
#vid = '3D/higherAngle.mp4'
#vid = '3D/empty.mp4'
main()
