from __future__ import print_function
import cv2 as cv
import argparse
import pathlib
import os

ROOT = pathlib.Path(__file__).parent.parent.resolve()
INPUT = ROOT.joinpath("input")
OUTPUT = ROOT.joinpath("output")
max_value = 255
max_value_H = 360//2
low_H = 0
low_S = 0
low_V = 0
#low_A = 0
high_H = max_value
high_S = max_value
high_V = max_value
#high_A = max_value
window_capture_name = 'Video Capture'
window_detection_name = 'Object Detection'
low_H_name = 'Low H'
low_S_name = 'Low S'
low_V_name = 'Low V'
#low_A_name = 'Low A'
high_H_name = 'High H'
high_S_name = 'High S'
high_V_name = 'High V'
#high_A_name = 'High A'
def on_low_H_thresh_trackbar(val):
    global low_H
    global high_H
    low_H = val
    low_H = min(high_H-1, low_H)
    cv.setTrackbarPos(low_H_name, window_detection_name, low_H)
def on_high_H_thresh_trackbar(val):
    global low_H
    global high_H
    high_H = val
    high_H = max(high_H, low_H+1)
    cv.setTrackbarPos(high_H_name, window_detection_name, high_H)
def on_low_S_thresh_trackbar(val):
    global low_S
    global high_S
    low_S = val
    low_S = min(high_S-1, low_S)
    cv.setTrackbarPos(low_S_name, window_detection_name, low_S)
def on_high_S_thresh_trackbar(val):
    global low_S
    global high_S
    high_S = val
    high_S = max(high_S, low_S+1)
    cv.setTrackbarPos(high_S_name, window_detection_name, high_S)
def on_low_V_thresh_trackbar(val):
    global low_V
    global high_V
    low_V = val
    low_V = min(high_V-1, low_V)
    cv.setTrackbarPos(low_V_name, window_detection_name, low_V)
def on_high_V_thresh_trackbar(val):
    global low_V
    global high_V
    high_V = val
    high_V = max(high_V, low_V+1)
    cv.setTrackbarPos(high_V_name, window_detection_name, high_V)
# def on_low_A_thresh_trackbar(val):
#     global low_A
#     global high_A
#     low_A = val
#     low_A = min(high_A-1, low_A)
#     cv.setTrackbarPos(low_A_name, window_detection_name, low_A)
# def on_high_A_thresh_trackbar(val):
#     global low_A
#     global high_A
#     high_A = val
#     high_A = max(high_A, low_A+1)
#     cv.setTrackbarPos(high_A_name, window_detection_name, high_A)
parser = argparse.ArgumentParser(description='Code for Thresholding Operations using inRange tutorial.')
parser.add_argument('--camera', help='Camera divide number.', default=0, type=int)
#args = parser.parse_args()
#cap = cv.VideoCapture(args.camera)
#cap = cv.VideoCapture(os.path.join(INPUT, "3D/higherAngle.mp4"))
#cap = cv.VideoCapture(os.path.join(INPUT, "2D/2d_board.mp4"))
cap = cv.VideoCapture(os.path.join(INPUT, "2D/exampleGame.avi"))
cv.namedWindow(window_capture_name)
cv.namedWindow(window_detection_name)
cv.createTrackbar(low_H_name, window_detection_name , low_H, max_value, on_low_H_thresh_trackbar)
cv.createTrackbar(high_H_name, window_detection_name , high_H, max_value, on_high_H_thresh_trackbar)
cv.createTrackbar(low_S_name, window_detection_name , low_S, max_value, on_low_S_thresh_trackbar)
cv.createTrackbar(high_S_name, window_detection_name , high_S, max_value, on_high_S_thresh_trackbar)
cv.createTrackbar(low_V_name, window_detection_name , low_V, max_value, on_low_V_thresh_trackbar)
cv.createTrackbar(high_V_name, window_detection_name , high_V, max_value, on_high_V_thresh_trackbar)
#cv.createTrackbar(low_A_name, window_detection_name , low_A, max_value, on_low_A_thresh_trackbar)
#cv.createTrackbar(high_A_name, window_detection_name , high_A, max_value, on_high_A_thresh_trackbar)
scale_percent = 100 # percent of original size
width = int(722 * scale_percent / 100)
height = int(722 * scale_percent / 100)
dim = (width, height)
  

while True:
    
    ret, frame = cap.read()
    if frame is None:
        break
    # resize image
    frame_downscale = cv.resize(frame, dim, interpolation = cv.INTER_AREA)
    #frame_HSV = cv.cvtColor(frame, cv.COLOR_BGR2BGRA)
    frame_threshold = cv.inRange(frame_downscale, (low_H, low_S, low_V), (high_H, high_S, high_V))
    #frame_threshold = cv.inRange(frame, (low_H, low_S, low_V), (high_H, high_S, high_V))
    
    
    cv.imshow(window_capture_name, frame)
    cv.imshow(window_detection_name, frame_threshold)
    
    key = cv.waitKey(0)
    if key == ord('q') or key == 27:
        break