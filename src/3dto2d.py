import chess
import cv2
import pathlib
import os
import numpy as np
import glob
ROOT = pathlib.Path(__file__).parent.parent.resolve()
INPUT = ROOT.joinpath("input")

class ChessboardNotFoundError(Exception):
    pass
size = (640,480)

def get_chessboard(vid):
    cap = cv2.VideoCapture(os.path.join(INPUT, vid))
    if (cap.isOpened()==False):
        print("can't read")
    col = 3
    row = 7
    while(cap.isOpened()):
        # Capture frame-by-frame
        ret, frame = cap.read()
        ddepth = cv2.CV_16S
        kernel = np.array([[0, -1, 0],
                   [-1, 5,-1],
                   [0, -1, 0]])
        kernel_size = 3
        if ret == True:
            # Color-segmentation to get binary mask
            # lwr = np.array([13, 0, 0])
            # upr = np.array([99, 255, 255])
            # hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2YUV)

            lwr = np.array([0, 0, 0])
            upr = np.array([95, 255, 255])
            lab = cv2.cvtColor(frame, cv2.COLOR_BGR2LAB)

            lwr2 = np.array([80, 80, 140])
            upr2 = np.array([255, 255, 255])
            #lab = cv2.cvtColor(frame, cv2.COLOR_BGR2BGR555)


            #cv2.imshow("hsv",hsv)
            #cv2.waitKey(0)
            msk1 = cv2.inRange(lab, lwr, upr)
            msk2 = cv2.inRange(frame, lwr2, upr2)

            # Extract chess-board
            krn = cv2.getStructuringElement(cv2.MORPH_RECT, (20, 20))
            dlt1 = cv2.dilate(msk1, krn, iterations=5)
            dlt2 = cv2.dilate(msk2, krn, iterations=5)
            res1 = 255 - cv2.bitwise_and(dlt1, msk1)
            res2 = 255 - cv2.bitwise_and(dlt2, msk2)
            res = res1 - res2
            res = np.uint8(res)
            #dst = cv2.Laplacian(res, ddepth, ksize=kernel_size)
            #abs_dst = cv2.convertScaleAbs(dst)
            image_sharp = cv2.filter2D(src=res, ddepth=-1, kernel=kernel)
            cool = image_sharp + res

            

            # Displaying chess-board features
            # cv2.imshow("res",res)
            #cv2.imshow("cool",cool)

            # cv2.waitKey(0)
            ret, corners = cv2.findChessboardCorners(cool, (col, row),
                                                    flags=cv2.CALIB_CB_ADAPTIVE_THRESH +
                                                        cv2.CALIB_CB_FAST_CHECK +
                                                        cv2.CALIB_CB_NORMALIZE_IMAGE)
            if ret:
                print(corners)
                #fnl = cv2.drawChessboardCorners(frame, (col, row), corners, ret)
                #cv2.imshow("fnl", fnl)
                #cv2.imshow("hsv",msk2)
                #cv2.imshow("res",res)
                #cv2.waitKey(0)
                return corners
            else:
                print("No Checkerboard Found")
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break

        # Break the loop
        else: 
            break

### A better code, might implement later ###
# def _get_corners(image):
#         """Find subpixel chessboard corners in image."""
#         temp = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#         ret, corners = cv2.findChessboardCorners(temp,
#                                                  (3, 7))
#         if not ret:
#             raise ChessboardNotFoundError("No chessboard could be found.")
#         cv2.cornerSubPix(temp, corners, (11, 11), (-1, -1),
#                          (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS,
#                           30, 0.01))
#         return corners, ret

# fnl = cv2.drawChessboardCorners(file, (3, 7), corn, ret)

def remDimension(vid, grid):
    procGrid = np.array_split(grid,7)
    #print(procGrid[0][1][0][0])

    #get the top point between H5 & H4
    if procGrid[0][1][0][0] < procGrid[1][1][0][0]:
        ori = -1
    else: 
        ori = 1
    topDiffX = procGrid[0][1][0][0] - ((procGrid[0][1][0][0]-procGrid[1][1][0][0])*ori)
    topDiffY = procGrid[0][1][0][1] - (procGrid[1][1][0][1]-procGrid[0][1][0][1])
    top = (int(topDiffX), int(topDiffY))
    print(top)
    image = cv2.circle(vid, top, radius=5, color=(0, 0, 255), thickness=1)
    cv2.imshow("topPoint", image)
    cv2.waitKey(0)
    

vid = '3D/higherAngle.mp4'
firstFrame = INPUT.joinpath("3D","higherAngle_Moment.jpg")
firsty = cv2.imread(str(firstFrame))
birdseye_dir = INPUT.joinpath("2D","2d_board_Moment.jpg")
grid = get_chessboard(vid)
grid37 = np.array_split(grid,7)
#remDimension(firsty,grid37)
if __name__ == '__main__' :

    # Read source image.
    im_src = firsty
    # Four corners of the book in source image
    pts_src = np.array([grid37[0][2][0], grid37[0][0][0], grid37[6][2][0],grid37[6][0][0]])

    # Read destination image.
    im_dst = cv2.imread(str(birdseye_dir))
    # Four corners of the book in destination image.
    pts_dst = np.array([[668, 207],[670, 335],[289, 206],[290, 331]])

    # Calculate Homography
    h, status = cv2.findHomography(pts_src, pts_dst)

    # Warp source image to destination based on homography
    im_out = cv2.warpPerspective(im_src, h, (im_dst.shape[1],im_dst.shape[0]))

    # Display images
    cv2.imshow("Source Image", im_src)
    cv2.imshow("Destination Image", im_dst)
    cv2.imshow("Warped Source Image", im_out)

    cv2.waitKey(0)

