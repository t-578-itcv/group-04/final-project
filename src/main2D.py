import chess
import cv2
import pathlib
import os
import numpy as np
ROOT = pathlib.Path(__file__).parent.parent.resolve()
INPUT = ROOT.joinpath("input/2D")

COLUMNS = 8
ROWS = 8
# size = (640,480)

def get_chessboard_coordinates(frame, size):
    col,row = size
    lwr = np.array([64, 0, 0])
    upr = np.array([192, 128, 224])
    msk = cv2.inRange(frame, lwr, upr)
    ret, corners = cv2.findChessboardCorners(msk, (col, row),\
        flags=cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_FAST_CHECK + cv2.CALIB_CB_NORMALIZE_IMAGE)
    if ret:
        if corners.shape == (col*row, 1, 2):
            return True, corners
        else:
            print('? :', corners.shape, corners)
            return False, corners
        # print(corners, corners.shape)
        # fnl = cv2.drawChessboardCorners(frame, (col, row), corners, ret)
        # cv2.imshow("fnl", fnl)
        # cv2.waitKey(0)
    else:
        print("No Checkerboard Found")

def euclidian_dist(z1,z2):
    x1,y1 = z1
    x2,y2 = z2
    return np.sqrt((x1-x2)**2+(y1-y2)**2)

def get_avrg_dist(corners, size):
    col, row = size
    max_dim = max(col, row)
    min_dim = min(col, row)
    if col == max_dim:
        index = 0
    else: 
        index = 1
    avrgs = 0
    for i in range(min_dim):
        _avrg = 0
        for j in range(max_dim):
            dist = abs(corners[i*max_dim][0][index]-corners[i*max_dim+j][0][index])
            _avrg += 2*dist/max_dim/(max_dim-1)
        avrgs += _avrg/min_dim
    return round(avrgs)

def difference(img1, img2):
    return 1

def crop_to_compare(frame, previous_frame, dist, center):
    x,y = center
    x,y = int(x), int(y)
    size = COLUMNS//2
    x_max = x+size*dist
    x_min = x-size*dist
    y_max = y+size*dist
    y_min = y-size*dist
    cv2.imshow('frame', frame)
    frame = frame[y_min:y_max,x_min:x_max]
    previous_frame = previous_frame[x_min:x_max,y_min:y_max]
    cv2.imshow('crop', frame)
    cv2.waitKey()
    return frame, previous_frame

def compare(input, th1, th2):
    frame, previous_frame= input
    diff = difference(frame, previous_frame)
    if th1<=diff<=th2:
        return True
    else:
        return False

def get_chessboard(vid):
    cap = cv2.VideoCapture(os.path.join(INPUT, vid))
    if (cap.isOpened()==False):
        print("can't read")
    # opponents are facing along the North-South axis not West-East
    col = 7
    row = 3
    th1, th2 = (1,100) # threshold values for move detection
    board_found = False
    move_transcript = []
    while(cap.isOpened()):
        # Capture frame-by-frame
        ret, frame = cap.read()
        if board_found:
            is_move = compare(crop_to_compare(frame, previous_frame, dist, center), th1, th2)
            break
            if is_move:
                move = find_move_coord(frame, previous_frame, dist)
                move_transcript.append(move)
            previous_frame = frame
        else:
            if ret == True:
                board_found, corners = get_chessboard_coordinates(frame, (col,row))
                dist = get_avrg_dist(corners, (col, row))
                center = corners[(col*row-1)//2][0]
                previous_frame = frame
            # Break the loop
            else: 
                print('ret is false for read in get_chessboard')
                break

vid = '2d_board.mp4'
get_chessboard(vid)

