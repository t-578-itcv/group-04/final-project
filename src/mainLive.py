import chess
import cv2
import pathlib
import os
import numpy as np
import keyboard
import pyttsx3
import time
from skimage.metrics import structural_similarity as compare_ssim
from itertools import permutations

ROOT = pathlib.Path(__file__).parent.parent.resolve()
INPUT = ROOT.joinpath("input")
#birdseye_dir = INPUT.joinpath("2D","2d_board_Moment.jpg")
# define the empty chessboard template for the warping2D
birdseye_dir = INPUT.joinpath("2D","clean_board.png")

############## getting to 2D #######################

def warp2D(grid37, im_src, im_dst):
    # Four corners of the book in source image
    pts_src = np.array([grid37[0][0][0], grid37[0][2][0], grid37[6][0][0],grid37[6][2][0]])
    # Four corners of the book in destination image.
    pts_dst = np.array([[91, 450],[91, 270],[631, 450],[631, 270]])
    # Calculate Homography
    h, status = cv2.findHomography(pts_src, pts_dst)
    # Warp source image to destination based on homography
    im_out = cv2.warpPerspective(im_src, h, (im_dst.shape[1],im_dst.shape[0]))
    # Display images
    #cv2.imshow("Source Image", im_src)
    #cv2.imshow("Destination Image", im_dst)
    #cv2.imshow("Warped Source Image", im_out)
    return im_out

################# detecting the chessboard ##################

def detect_chessboard(cap, ret1):
    while(cap.isOpened() & ret1==False):
        # Capture frame-by-frame
        ret, frame = cap.read()
        # not for gitlab pipeline
        cv2.imshow("raw footage", frame)
        cv2.waitKey(25)
        """Find subpixel chessboard corners in image."""
        temp = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        ret1, corners = cv2.findChessboardCorners(temp,
                                                 (3, 7))
        if not ret1:
            #raise ChessboardNotFoundError("No chessboard could be found.")
            continue
        cv2.cornerSubPix(temp, corners, (11, 11), (-1, -1),
                         (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS,
                          30, 0.01))
        #fnl = cv2.drawChessboardCorners(frame, (3, 7), corners, ret)
    grid37 = np.array_split(corners,7)
    im_dst = cv2.imread(str(birdseye_dir))
    return grid37, im_dst

##### finding the move made ##############

def generateBoard(width):
    gap = width // 8
    squares = {}
    hori = ['a','b','c','d','e','f','g','h']
    vert = ['8','7','6','5','4','3','2','1']
    squareCol = 0
    for i in range(8):
        for j in range(8):
            if squareCol % 2 == 0:
                white = True
            else:
                white = False
            tl= (gap*i, gap*j)
            br = (gap+gap*i, gap+gap*j)
            id = str(hori[i]+vert[j])
            squares[id] = (tl, br),white
            squareCol += 1
    return squares

def detectDiff(imageA, imageB, whiteSquare = None, whitesTurn = None):
    # load the two input images
    # convert the images to grayscale
    frame_LUVA = cv2.cvtColor(imageA, cv2.COLOR_BGR2LUV)
    frame_thresholdA = cv2.inRange(frame_LUVA, (25, 0, 0), (100, 255, 255))
    frame_LUVB = cv2.cvtColor(imageB, cv2.COLOR_BGR2LUV)
    frame_thresholdB = cv2.inRange(frame_LUVB, (25, 0, 0), (100, 255, 255))

    hsvA = cv2.cvtColor(imageA, cv2.COLOR_BGR2HSV)
    hsvB = cv2.cvtColor(imageB, cv2.COLOR_BGR2HSV)
    grayA = cv2.cvtColor(hsvA, cv2.COLOR_BGR2GRAY)
    grayB = cv2.cvtColor(hsvB, cv2.COLOR_BGR2GRAY)
    # compute the Structural Similarity Index (SSIM) between the two
    # images, ensuring that the difference image is returned
    score1, diff = compare_ssim(frame_thresholdA, frame_thresholdB, full=True)
    score2, diff = compare_ssim(grayA, grayB, full=True)
    return score1, score2

def analyzeSquares(squares, img):
    indivudualSquares = {}
    for key in squares.keys():
        white = squares[key][1]
        tl = (squares[key][0][0])
        br = (squares[key][0][1])
        #cent = (tl[0]+45, tl[1]+45)
        crop_img = img[tl[1]:br[1], tl[0]:br[0]]
        #crop_img = cv2.cvtColor(crop_img,cv2.COLOR_BGR2HSV)
        indivudualSquares[key] = (crop_img, white) 
    return indivudualSquares

def determine_which_movement(squares, frame, previous_positions, board, all_moves_made, all_moves_made_str, engine):
    positions = analyzeSquares(squares, frame)
    scores_arr = []
    # if turn %2 == 0:
    #     whitesTurn = True
    # else:
    #     whitesTurn = False
    whitesTurn = True # not used in detectDiff for now

    for key in previous_positions:
        score1, score2 = detectDiff(previous_positions[key][0],positions[key][0], previous_positions[key][1],whitesTurn)
        if score1 < score2:
            bigger = score2
        else:
            bigger = score1
        tup = (bigger, key)
        scores_arr.append(tup)

    scores_arr.sort(key=lambda x:x[0])
    # print("turn {}".format(1))
    top_10=[]
    for j in range(30):
        top_10.append(scores_arr[j][1])
    # Get all permutations of [1, 2, 3]
    index = 1
    no_move_found = True
    while index<=29 and no_move_found:
        index+=1
        perm = permutations(top_10[:index], 2)
        for p in perm:
            move = (str(p[0])+str(p[1]))
            if chess.Move.from_uci(move) in board.legal_moves:
                firstSquare = move[0:2]
                secondSquare = move[2:4]
                all_moves_made.append(move)
                movedPiece = board.piece_at(chess.parse_square(firstSquare))
                #print(movedPiece.color)
                movedName = chess.piece_name(movedPiece.piece_type)
                board.push_san(move)
                moveMade = movedName +" from " +firstSquare + " to "+secondSquare
                all_moves_made_str += moveMade+'\n'
                engine.say(moveMade)
                print(moveMade)
                engine.runAndWait()
                no_move_found = False
                break

    if not no_move_found:
        # prepare for next move
        previous_state = frame
        previous_positions = positions
    else:
        print('no_move_found')
    return squares, frame, previous_state, previous_positions, board, all_moves_made, all_moves_made_str
            
########### detect movement yes or no #############

def getScores(img1, img2, squares):
    scores_arr = []
    positions1 = analyzeSquares(squares, img1)
    positions2 = analyzeSquares(squares, img2)
    for key in positions1:
        score1, score2 = detectDiff(positions1[key],positions2[key])
        if score1 < score2:
            bigger = score2
        else:
            bigger = score1
        tup = (bigger, key)
        scores_arr.append(tup)
    return scores_arr

def detectMove(prev, curr, squares):
    scores_arr = getScores(prev,curr,squares)
    sco = []
    for score in scores_arr:
        sco.append(score[0])
    avg = np.mean(sco)
    return avg

def detect_movement():
    # do something a bit similar to detectDiff, but different of course :)
    pass

######## main ############

def check_vid(vid):
    if vid:
        cap = cv2.VideoCapture(os.path.join(INPUT, vid))
    else:
        cap = cv2.VideoCapture(0)
    if (cap.isOpened()==False):
        print("can't read")
    return cap

def main(vid=None):
    cap = check_vid(vid)

    ret1 = False
    grid37, im_dst = detect_chessboard(cap, ret1)

    cap = check_vid(vid) # maybe unnecessery ?
    
    # initialize variables
    firstLoop = True
    # black_turn = False
    # white_turn = True
    engine = pyttsx3.init()
    board = chess.Board()
    squares = generateBoard(722)
    all_moves_made_str = ''
    all_moves_made = []
    # turn = 1 # if this is used, add it to determine_which_movement input and uncomment inside

    # wait 5 sec to take into account lighting change
    time.sleep(5)
    
    while(cap.isOpened()):
        
        
        # Capture frame-by-frame
        ret, frame = cap.read()
        if ret == True:
            # getting the starting state (only in the first loop)
            if firstLoop == True:
                birdseyeView = warp2D(grid37,frame,im_dst)
                previous_state = birdseyeView #needed for movement detection boolean
                previous_positions = analyzeSquares(squares, birdseyeView)
                firstLoop = False
            else:

                birdseyeView = warp2D(grid37,frame,im_dst)
                # not for gitlab pipeline
                cv2.imshow("raw footage", frame)
                cv2.imshow("birdseye",birdseyeView)

            # try:  # used try so that if user pressed other than the given key error will not be shown
            #     if keyboard.is_pressed('z') and black_turn:  # if key 'q' is pressed 
            #         black_turn = False
            #         white_turn = True
            #         boolean_for_movement = True
            #         print('You Pressed Z Key!')
            #         # finishing the loop
            #     if keyboard.is_pressed('0') and white_turn:
            #         white_turn = False
            #         black_turn = True
            #         boolean_for_movement = True
            #         print('You pressed 0 key!')
            # except:
            #     print("")
            
                stable = detectMove(previous_state, birdseyeView, squares)
                boolean_for_movement = stable > 0.95

                if boolean_for_movement:
                    boolean_for_movement = False
                    squares, birdseyeView, previous_state, previous_positions, board, all_moves_made, all_moves_made_str, engine =\
                        determine_which_movement(squares, birdseyeView, previous_positions, board, all_moves_made, all_moves_made_str, engine)

                # there is a checkmate OR we want to stop and erase
                if cv2.waitKey(1000) & 0xFF == ord('q'):
                    break

        else:
            break
    cap.release()

#vid = '3D/higherAngle.mp4'
#vid = '3D/empty.mp4'
# vid = '2D/tournament.avi'
main()
