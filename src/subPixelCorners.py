import numpy as np
import cv2
class ChessboardNotFoundError(Exception):
    pass
def _get_corners(image):
        """Find subpixel chessboard corners in image."""
        temp = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        ret, corners = cv2.findChessboardCorners(temp,
                                                 (3, 7))
        if not ret:
            raise ChessboardNotFoundError("No chessboard could be found.")
        cv2.cornerSubPix(temp, corners, (11, 11), (-1, -1),
                         (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS,
                          30, 0.01))
        return corners, ret
filename = 'input/3D/higherAngle_Moment.jpg'
file = cv2.imread(filename)
corn, ret = _get_corners(file)
print(corn)
fnl = cv2.drawChessboardCorners(file, (3, 7), corn, ret)
cv2.imshow("final", fnl)
cv2.waitKey(0)