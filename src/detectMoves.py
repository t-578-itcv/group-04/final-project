import chess
import cv2
import pathlib
import numpy as np
import pyttsx3
from skimage.metrics import structural_similarity as compare_ssim
from itertools import permutations

ROOT = pathlib.Path(__file__).parent.parent.resolve()
INPUT = ROOT.joinpath("input")
#in_dir = str(INPUT.joinpath("2D", "exampleGame.avi"))
in_dir = str(INPUT.joinpath("2D", "tournament.avi"))

def detectDiff(imageA,imageB, whiteSquare=None, whitesTurn =None):
    # load the two input images
    # convert the images to grayscale
    frame_LUVA = cv2.cvtColor(imageA, cv2.COLOR_BGR2LUV)
    frame_thresholdA = cv2.inRange(frame_LUVA, (25, 0, 0), (100, 255, 255))
    frame_LUVB = cv2.cvtColor(imageB, cv2.COLOR_BGR2LUV)
    frame_thresholdB = cv2.inRange(frame_LUVB, (25, 0, 0), (100, 255, 255))

    hsvA = cv2.cvtColor(imageA, cv2.COLOR_BGR2HSV)
    hsvB = cv2.cvtColor(imageB, cv2.COLOR_BGR2HSV)
    grayA = cv2.cvtColor(hsvA, cv2.COLOR_BGR2GRAY)
    grayB = cv2.cvtColor(hsvB, cv2.COLOR_BGR2GRAY)
    # compute the Structural Similarity Index (SSIM) between the two
    # images, ensuring that the difference image is returned
    score1, diff = compare_ssim(frame_thresholdA, frame_thresholdB, full=True)
    score2, diff = compare_ssim(grayA, grayB, full=True)
    return score1, score2

def generateBoard(width):
    gap = width // 8
    squares = {}
    hori = ['a','b','c','d','e','f','g','h']
    vert = ['8','7','6','5','4','3','2','1']
    squareCol = 0
    for i in range(8):
        for j in range(8):
            if squareCol % 2 == 0:
                white = True
            else:
                white = False
            tl= (gap*i, gap*j)
            br = (gap+gap*i, gap+gap*j)
            id = str(hori[i]+vert[j])
            squares[id] = (tl, br),white
            squareCol += 1
    return squares
    
def analyzeSquares(squares, in_dir):
    cap = cv2.VideoCapture(in_dir)
    #font = cv2.FONT_HERSHEY_SIMPLEX
    positions = []
    while (cap.isOpened()):
        ret, frame = cap.read()
        if ret == True:
            indivudualSquares = {}
            for key in squares.keys():
                white = squares[key][1]
                tl = (squares[key][0][0])
                br = (squares[key][0][1])
                #cent = (tl[0]+45, tl[1]+45)
                crop_img = frame[tl[1]:br[1], tl[0]:br[0]]
                #crop_img = cv2.cvtColor(crop_img,cv2.COLOR_BGR2HSV)
                indivudualSquares[key] = (crop_img, white) 

                #cv2.putText(frame, key, cent, font, 1, (0, 255, 0), 2, cv2.LINE_AA)
            #cv2.imshow("frame", frame)
            #cv2.imshow("A8", indivudualSquares["A8"])
            #cv2.imshow("H8", indivudualSquares["H8"])
            positions.append(indivudualSquares)
            #cv2.waitKey(1000)
        else:
            break
    cap.release()
    cv2.destroyAllWindows()
    return positions


if __name__ == "__main__":
    engine = pyttsx3.init()
    board = chess.Board()
    squares = generateBoard(722)
    positions = analyzeSquares(squares, in_dir)
    scores_arr_arr = []
    # turn = 1
    for i in range(0,len(positions)-1): 
        scores_arr = []
        # if turn %2 == 0:
        #     whitesTurn = True
        # else:
        #     whitesTurn = False
        whitesTurn = False

        #print("turn {}: ".format(i))
        for key in positions[i]:
            score1, score2 = detectDiff(positions[i][key][0],positions[i+1][key][0], positions[i][key][1],whitesTurn)
            if score1 < score2:
                bigger = score2
            else:
                bigger = score1
            tup = (bigger, key)
            scores_arr.append(tup)
        scores_arr_arr.append(scores_arr)
    for i in range(len(scores_arr_arr)):
        scores_arr_arr[i].sort(key=lambda x:x[0])
        print("turn {}".format(i))
        # if i == 0:
        #     print(scores_arr_arr)
        top_10=[]
        for j in range(30):
            top_10.append(scores_arr_arr[i][j][1])
        # Get all permutations of [1, 2, 3]
        index = 1
        no_move_found = True
        legal_perms = []
        while index<=29 and no_move_found:
            index+=1
            perm = permutations(top_10[:index], 2)
            legal_perms = []
            for p in perm:
                move1 = (str(p[0])+str(p[1]))
                if chess.Move.from_uci(move1) in board.legal_moves:
                    legal_perms.append(move1)
                    firstSquare = move1[0:2]
                    secondSquare = move1[2:4]
                    movedPiece = board.piece_at(chess.parse_square(firstSquare))
                    #print(movedPiece.color)
                    movedName = chess.piece_name(movedPiece.piece_type)
                    board.push_san(move1)
                    moveMade = movedName +" from " +firstSquare + " to "+secondSquare
                    engine.say(moveMade)
                    print(moveMade)
                    engine.runAndWait()
                    no_move_found = False
                    break
        
        #print(legal_perms)
