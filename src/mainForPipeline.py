import chess
import cv2
import pathlib
import os
import numpy as np
from skimage.metrics import structural_similarity as compare_ssim
from itertools import permutations

# define directories
ROOT = pathlib.Path(__file__).parent.parent.resolve()
INPUT = ROOT.joinpath("input")
OUTPUT = ROOT.joinpath("output")
# import the image of a 8x8 chessboard grid
birdseye_dir = INPUT.joinpath("2D","clean_board.png")

class ChessboardNotFoundError(Exception):
    pass

def cropSquares(squares, frame):
    # splits the image into its 64 squares
    # saves the squares as a dictionary with its coordinates as key
    indivudualSquares = {}
    for key in squares.keys():
        tl = (squares[key][0])
        br = (squares[key][1])
        crop_img = frame[tl[1]:br[1], tl[0]:br[0]]
        indivudualSquares[key] = crop_img

    return indivudualSquares

def generateBoard(width):
    # maps out the chessboard with the top-left and bottom-right corner
    # of each square, saves the squares as a dictionary with grid coordinates as keys
    gap = width // 8
    squares = {}
    hori = ['a','b','c','d','e','f','g','h']
    vert = ['8','7','6','5','4','3','2','1']
    for i in range(8):
        for j in range(8):
            tl= (gap*i, gap*j)
            br = (gap+gap*i, gap+gap*j)
            id = str(hori[i]+vert[j])
            squares[id] = (tl, br)
    return squares

def detectDiff(imageA,imageB):
    # load the two input images and process them in two different ways 
    
    # the LUV filter is better at finding black pieces in black squares
    # converts to LUV and uses a threshold to seperate dark colors
    frame_LUVA = cv2.cvtColor(imageA, cv2.COLOR_BGR2LUV)
    frame_thresholdA = cv2.inRange(frame_LUVA, (25, 0, 0), (100, 255, 255))
    frame_LUVB = cv2.cvtColor(imageB, cv2.COLOR_BGR2LUV)
    frame_thresholdB = cv2.inRange(frame_LUVB, (25, 0, 0), (100, 255, 255))

    # the HSV filter is better at finding white pieces in white squares
    hsvA = cv2.cvtColor(imageA, cv2.COLOR_BGR2HSV)
    hsvB = cv2.cvtColor(imageB, cv2.COLOR_BGR2HSV)
    # convert the images to grayscale
    grayA = cv2.cvtColor(hsvA, cv2.COLOR_BGR2GRAY)
    grayB = cv2.cvtColor(hsvB, cv2.COLOR_BGR2GRAY)

    # compute the Structural Similarity Index (SSIM) between the two
    # images using both filters to get different results
    score1, diff = compare_ssim(frame_thresholdA, frame_thresholdB, full=True)
    score2, diff = compare_ssim(grayA, grayB, full=True)
    return score1, score2

def getScores(img1, img2, squares):
    scores_arr = []
    # get the individual squares of foth frames
    positions1 = cropSquares(squares, img1)
    positions2 = cropSquares(squares, img2)
    for key in positions1:
        score1, score2 = detectDiff(positions1[key],positions2[key])
        # since one filter is better than the other in certain positions
        # get the bigger score because bigger is better
        if score1 < score2:
            bigger = score2
        else:
            bigger = score1
        tup = (bigger, key)
        scores_arr.append(tup)
    return scores_arr

def getMove(scores_arr, board):
    scores_arr.sort(key=lambda x:x[0])
    top_10=[]
    # get the lowest difference scores on the board
    for j in range(30):
        top_10.append(scores_arr[j][1])
    index = 1
    no_move_found = True
    legal_perms = []
    moveMade = None
    # stop looking once we've invesigated half the board
    while index<=29 and no_move_found:
        index+=1
        perm = permutations(top_10[:index], 2)
        legal_perms = []
        # get all the permutations for the squares we're considering
        for p in perm:
            move = (str(p[0])+str(p[1]))
            # make the move once we've found a legal move in the permutations
            if chess.Move.from_uci(move) in board.legal_moves:
                legal_perms.append(move)
                firstSquare = move[0:2]
                secondSquare = move[2:4]
                movedPiece = board.piece_at(chess.parse_square(firstSquare))
                movedName = chess.piece_name(movedPiece.piece_type)
                moveMade = movedName +" from " +firstSquare + " to "+secondSquare
                no_move_found = False
                break
    if not moveMade:
        return None
    else:
        board.push_san(move)
        return moveMade

def detectMove(prev, curr, squares):
    # used for figuring out if there is movement on the field
    # returns the mean difference score for each frame
    scores_arr = getScores(prev,curr,squares)
    sco = []
    for score in scores_arr:
        sco.append(score[0])
    avg = np.mean(sco)
    return avg

def warp2D(grid37, im_src, im_dst):
    # Four corners of the middle of the board in raw footage
    pts_src = np.array([grid37[0][0][0], grid37[0][2][0], grid37[6][0][0],grid37[6][2][0]])
    # Four corners of the middle of the board in 2D image.
    pts_dst = np.array([[91, 450],[91, 270],[631, 450],[631, 270]])
    # Calculate Homography
    h, status = cv2.findHomography(pts_src, pts_dst)
    # Warp source image to destination based on homography
    im_out = cv2.warpPerspective(im_src, h, (im_dst.shape[1],im_dst.shape[0]))

    return im_out

def main(vid=None):
    if vid:
        cap = cv2.VideoCapture(os.path.join(INPUT, vid))
    else:
        cap = cv2.VideoCapture(0)
    if (cap.isOpened()==False):
        print("can't read")
    ret1 = False
    while(cap.isOpened() & ret1==False):
        # Capture frame-by-frame
        ret, frame = cap.read()
        """Find subpixel chessboard corners in image."""
        temp = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        ret1, corners = cv2.findChessboardCorners(temp,
                                                 (3, 7))
        if not ret1:
            #raise ChessboardNotFoundError("No chessboard could be found.")
            continue
        cv2.cornerSubPix(temp, corners, (11, 11), (-1, -1),
                         (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS,
                          30, 0.01))
    # the grid coordinates used for perspective transformation
    # when using the code live, there is a first main loop to calibrate this
    #grid37 = ([np.array([[[280.7889 ,  96.69955]],[[323.60187,  98.55817]],[[365.8586 , 101.14888]]]), np.array([[[277.31677 , 122.61504 ]],[[321.1874  , 125.077446]],[[365.55432 , 127.29663 ]]]), np.array([[[273.71774, 150.32307]],[[319.28186, 152.97566]],[[365.76224, 155.5242 ]]]), np.array([[[269.33932, 182.17754]],[[316.59735, 184.54271]],[[365.58624, 187.25609]]]), np.array([[[264.52676, 215.42563]],[[313.66226, 217.92052]],[[365.65   , 220.48535]]]), np.array([[[259.73767, 252.23248]],[[311.232  , 254.26207]],[[365.52655, 258.09457]]]), np.array([[[253.78178, 292.3969 ]],[[307.57263, 295.58838]],[[364.60538, 299.35257]]])])
    grid37 = np.array_split(corners,7)
    # the 2D board we're projecting onto
    im_dst = cv2.imread(str(birdseye_dir))
    if vid:
        cap = cv2.VideoCapture(os.path.join(INPUT, vid))
    else:
        cap = cv2.VideoCapture(0)
    if (cap.isOpened()==False):
        print("can't read")

    firstLoop = True
    out = cv2.VideoWriter('output/finalResult.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 1, (722,722))
    squares = generateBoard(722)
    board = chess.Board()
    turn = 1
    gameText = ""
    while(cap.isOpened()):
        # Capture frame-by-frame
        if firstLoop == True:
            # use the first frame as the first image it's comparing to for finding moves
            #ret, frame = cap.read()
            birdseyeView = warp2D(grid37,frame,im_dst)
            lastMove = birdseyeView
            moving = False
            prevFrame = birdseyeView
            firstLoop = False
            out.write(birdseyeView)
            #raw.write(frame)
        else:
            ret, frame = cap.read()
            if ret == True:
                # get the birds eye view
                birdseyeView = warp2D(grid37,frame,im_dst)
                # get the stability factor
                stable = detectMove(prevFrame, birdseyeView, squares)

                ### for calibration ###
                # print(stable)

                # only record a move if the board is stable and no movement detected
                # this threshold must be calibrated for each lighting condition
                if not moving and stable < 0.81:
                    moving = True
                if moving and stable > 0.87:
                    moving = False
                    scores_arr = getScores(lastMove, birdseyeView, squares)
                    moveMade = getMove(scores_arr, board)
                    if moveMade == None:
                        pass
                    else:
                        gameText += "Turn {}:".format(turn)+"\n"
                        gameText += str(board)+"\n"
                        gameText += str(moveMade)+"\n"
                        turn += 1
                        lastMove = birdseyeView
                if board.is_checkmate():
                    gameText += "Checkmate"
                    break
                # prevframe is used in the each loop to compare for stability factor
                prevFrame = birdseyeView
                # write the transformed frame to video
                out.write(birdseyeView)
                # uses the webcam and only records at 1 FPS
                if cv2.waitKey(1000) & 0xFF == ord('q'):
                    break
            else:
                break
    cap.release()
    out.release()
    
    entire_path = OUTPUT.joinpath('main_output.txt')
    with open(entire_path, 'w') as f:
        f.write(gameText)
vid = '3D/noKeyboardRaw.avi'
main(vid)
