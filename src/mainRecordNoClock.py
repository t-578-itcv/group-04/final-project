import chess
import cv2
import pathlib
import os
import numpy as np
import glob
import keyboard
import time

ROOT = pathlib.Path(__file__).parent.parent.resolve()
INPUT = ROOT.joinpath("input")
#birdseye_dir = INPUT.joinpath("2D","2d_board_Moment.jpg")
birdseye_dir = INPUT.joinpath("2D","clean_board.png")
class ChessboardNotFoundError(Exception):
    pass

def warp2D(grid37, im_src, im_dst):

    
    # Read source image.

    # Four corners of the book in source image
    pts_src = np.array([grid37[0][0][0], grid37[0][2][0], grid37[6][0][0],grid37[6][2][0]])

    # Read destination image.
    
    # Four corners of the book in destination image.
    #pts_dst = np.array([[668, 207],[670, 335],[289, 206],[290, 331]])
    pts_dst = np.array([[91, 450],[91, 270],[631, 450],[631, 270]])

    # Calculate Homography
    h, status = cv2.findHomography(pts_src, pts_dst)

    # Warp source image to destination based on homography
    im_out = cv2.warpPerspective(im_src, h, (im_dst.shape[1],im_dst.shape[0]))

    # Display images
    #cv2.imshow("Source Image", im_src)
    #cv2.imshow("Destination Image", im_dst)
    #cv2.imshow("Warped Source Image", im_out)
    return im_out

def define_squares(vid):
    pass

def main(vid=None):
    if vid:
        cap = cv2.VideoCapture(os.path.join(INPUT, vid))
    else:
        cap = cv2.VideoCapture(0)
    if (cap.isOpened()==False):
        print("can't read")
    ret1 = False
    while(cap.isOpened() & ret1==False):
        # Capture frame-by-frame
        ret, frame = cap.read()
        cv2.imshow("input", frame)
        cv2.waitKey(25)
        """Find subpixel chessboard corners in image."""
        temp = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        ret1, corners = cv2.findChessboardCorners(temp,
                                                 (3, 7))
        if not ret1:
            #raise ChessboardNotFoundError("No chessboard could be found.")
            continue
        cv2.cornerSubPix(temp, corners, (11, 11), (-1, -1),
                         (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS,
                          30, 0.01))
        #fnl = cv2.drawChessboardCorners(frame, (3, 7), corners, ret)
    grid37 = np.array_split(corners,7)
    im_dst = cv2.imread(str(birdseye_dir))
    if vid:
        cap = cv2.VideoCapture(os.path.join(INPUT, vid))
    else:
        cap = cv2.VideoCapture(0)
    if (cap.isOpened()==False):
        print("can't read")
    raw = cv2.VideoWriter('output/raw.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (640,480))
    out = cv2.VideoWriter('output/outpy.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (722,722))
    
    #last_movement = out
    white_turn = True
    black_turn = False
    firstLoop = True
    # try:  # used try so that if user pressed other than the given key error will not be shown
    #         if keyboard.is_pressed('z'):  # if key 'q' is pressed 
    #             print('You Pressed Z Key!')
    #               # finishing the loop
    #         if keyboard.is_pressed('0'):
    #             print('You pressed 0 key!')
    #     except:
    #         break 
    time.sleep(5)
    while(cap.isOpened()):
        # Capture frame-by-frame
        if firstLoop:
            birdseyeView = warp2D(grid37,frame,im_dst)
            out.write(birdseyeView)
        firstLoop = False
        ret, frame = cap.read()
        if ret == True:
            birdseyeView = warp2D(grid37,frame,im_dst)
            raw.write(frame)
            cv2.imshow("raw footage", frame)
            cv2.imshow("birdseye",birdseyeView)
            out.write(birdseyeView)

            # try:  # used try so that if user pressed other than the given key error will not be shown
            #     if keyboard.is_pressed('z') and black_turn:  # if key 'q' is pressed 
            #         black_turn = False
            #         white_turn = True
            #         out.write(birdseyeView)
            #         print('You Pressed Z Key!')
            #         # finishing the loop
            #     if keyboard.is_pressed('0') and white_turn:
            #         white_turn = False
            #         black_turn = True
            #         out.write(birdseyeView)
            #         print('You pressed 0 key!')
            # except:
            #     pass
        
            # out.write(birdseyeView)
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break
            # Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
            
        else:
            break
    raw.release()
    cap.release()
    out.release()

#vid = '3D/higherAngle.mp4'
#vid = '3D/empty.mp4'
main()