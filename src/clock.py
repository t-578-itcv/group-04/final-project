import keyboard  # using module keyboard
while True:  # making a loop
    try:  # used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('z'):  # if key 'q' is pressed 
            print('You Pressed Z Key!')
              # finishing the loop
        if keyboard.is_pressed('0'):
            print('You pressed 0 key!')
    except:
        break  # if user pressed a key other than the given key the loop will break
