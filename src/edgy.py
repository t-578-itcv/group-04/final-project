import chess
import cv2
import pathlib
import os
import numpy as np
import glob
ROOT = pathlib.Path(__file__).parent.parent.resolve()
INPUT = ROOT.joinpath("input")
OUTPUT = ROOT.joinpath("output")
vid_dir = OUTPUT.joinpath("outpy.avi")
vid = cv2.VideoCapture(vid_dir)
if (vid.isOpened()==False):
    print("can't read")
while(vid.isOpened()):
    # Capture frame-by-frame
    ret, frame = vid.read()
    